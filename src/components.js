export default (editor, opts = {}) => {
  const dc = editor.DomComponents
  const defaultType = dc.getType('default')
  const defaultModel = defaultType.model
  const defaultView = defaultType.view

  const cssc = editor.CssComposer

  const componentName = 'playbook-reveal'
  const styleName = componentName
  const emptyStyleName = `${styleName}--empty`

  const createCssStyles = () => {
    const css = `
      .${styleName} {
        position: relative;
        display: inline-block;
        vertical-align: top;
      }

      .${emptyStyleName} {
        width: 50px;
        height: 50px;
      }
    `
    cssc.getAll().add(css)
  }

  editor.TraitManager.addType('data-content-trait', {
    events: { blur: 'onChange' },
    onValueChange () {
      const traitModel = this.model
      const selectedComponent = this.target
      const inputValue = traitModel.get('value')

      const attrs = selectedComponent.getAttributes()
      attrs['data-content'] = inputValue
      selectedComponent.setAttributes(attrs)
    },

    onUpdate ({ elInput, component }) {
      const attrs = component.getAttributes()
      const inputValue = attrs['data-content'] || ''

      elInput.value = inputValue
    },

    getInputEl () {
      return document.createElement('textarea')
    }
  })

  dc.addType(`${componentName}`, {
    isComponent: function (el) {
      if (el.tagName && el.tagName.toLowerCase() === 'div' && el.hasAttribute &&
        el.hasAttribute('data-component-marker')) {
        console.log('Detecting playbook reveal component.')
        if (el.getAttribute('data-component-marker') === `${componentName}`) {
          console.log('Playbook reveal component detected ...')
          return { type: `${componentName}` }
        }
      }
    },
    model: {
      defaults: {
        name: 'Reveal',
        droppable: false,
        attributes: {
          'data-component-marker': `${componentName}`
        },
        components: [{
          tagName: 'div',
          attributes: {
            'data-component-marker': 'alternative-content'
          },
          name: 'Alternative Content'
        }, {
          tagName: 'div',
          attributes: {
            'data-component-marker': 'initial-content'
          },
          name: 'Initial Content'
        }
        ],
        classes: `${styleName}`,
        traits: [{
          type: 'select',
          name: 'data-trigger',
          label: 'Trigger',
          options: [
            { id: 'click', name: 'Click' },
            { id: 'focus', name: 'Focus' },
            { id: 'hover', name: 'Hover' }
          ]
        }
        ]
      },

      init () {
        const checkEmpty = function () {
          const empty = !this.components().length
          this[empty ? 'addClass' : 'removeClass'](`${emptyStyleName}`)
        }

        this.components().forEach(function (component) {
          const attrs = component.getAttributes()
          if (!attrs['data-component-marker']) {
            return
          }

          const self = component
          component.listenTo(self.components(), 'add remove', checkEmpty.bind(self))
          checkEmpty.apply(component)
        })
      }
    },
    view: defaultView.extend({
      init () {
        createCssStyles()
      }
    })
  })
}
